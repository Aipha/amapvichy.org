Title: Héloïse - Les Terres Noires
Slug: Heloise-Les-Terres-Noires
Authors: amapvichy
Summary: Oeufs et conserves

Le 9 octobre 2022, lors de la journée de convivialité de notre Amap, nous avons eu le plaisir de visiter la ferme des terres noires. Cette ferme de 2 ha installée à Saint-Rémy en Rollat, Joris et Héloïse l’ont créé en avril 2020.

Une grande partie des terres est consacrée au maraîchage biologique : 7000 mètres carrés dehors et 700 mètres carrés sous serres.
Ils produisent une grande variété de légumes de saison, et des plans maraîchers qu’ils vendent le vendredi soir à la ferme et le samedi matin au marché de Cusset.
Une partie des légumes est transformée par « Podarno » en conserves : soupes, sauces, confitures de légumes.

L’an passé, Héloïse a transformé une cave en champignonnière : Schiitakés et Pleurottes viennent compléter leurs étalages pendant une grande partie de l’année.
Quelques animaux : deux jeunes ânes et cinq moutons de « compagnie » entretiennent les zones non cultivées.
Et toute une troupe de chats, utiles aussi, car ils remplacent efficacement et agréablement les pièges à souris !
Trois serres, montées avec l’aide de leurs amis, permettent à Joris et Héloïse d’assurer une production régulière même pendant la saison froide .
Récemment ils ont complété leur installation par la construction d’un bâtiment abritant le tracteur, la chambre froide, le point de lavage de légumes et une zone de repos. L’achat de la chambre froide a été en partie financée par une subvention attribuée aux jeunes agriculteurs par le Feader( Fond européen agricole pour le développement durable).

Joris et Héloïse font partie d’un réseau de maraîchers qui leur permet de bénéficier régulièrement de formations, d’échanger et de se soutenir les uns les autres .

![Héloise](../../doc/heloise.jpg "Photo d'Héloise")

Les projets :
- 220 mètres carrés de serre en plus au printemps : une serre de légumes diversifiés et une petite serre pour les plans destinés à la vente.
- la création d’un poulailler dont les normes s’inspirent du cahier des charge Demeter :
Fin décembre,une centaine de poules pondeuses viendront s’installer dans ce poulailler quatre étoiles !


### Contrat

Vous pouvez télécharger les contrats au format [pdf] pour les imprimer et remplir en deux exemplaires.
Vous pouvez trouver [ici] les prix des conserves.

> Un exemplaire devra être remis au producteur sur le lieu de livraison de l'Amap, accompagné de son règlement. Vous pouvez effectuer le paiement avec plusieurs  chèques. Dans ce cas, veuillez noter au dos de chaque chèque la date d'encaissement souhaitée.

[pdf]: ../../doc/contrat_amap_heloise.pdf
[ici]:../../doc/prix_conserves_heloise.pdf