Title: Pisciculture du Moulin Piat
Slug: Moulin-Piat
Authors: amapvichy
Summary: Pisciculteurs à Ferrières sur Sichon, Laurent et Guillaume élèvent de manière extensive des truites essentiellement

Ils fournissent quelques grandes surfaces locales et assurent eux même la vente directe de poissons frais. C'est également eux maintenant qui conditionnent leur poisson (fumage, filets en sachet sous vide ...). Ils livrent les  Amapien-ne-s tous les 15 jours, chaque semaine impaire.


### Contrat

Vous pouvez télécharger les contrats au format [pdf] ou [rtf] (texte) pour les remplir en deux exemplaires ou vous rendre sur leur [site] Web.

> Un exemplaire devra être remis au producteur sur le lieu de livraison de l'Amap, accompagné de son règlement. Vous pouvez effectuer le paiement avec plusieurs  chèques. Dans ce cas, veuillez noter au dos de chaque chèque la date d'encaissement souhaitée.

[pdf]: ../doc/contrat_amap_pisciculture_du_moulin_piat.pdf
[rtf]: ../doc/contrat_amap_pisciculture_du_moulin_piat.rtf

[site]: https://www.pisciculturemoulinpiat.com
