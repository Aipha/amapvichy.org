Title: Matthieu Pritzy
Slug: Matthieu-Pritzy
Authors: amapvichy
Summary: Eleveur de chèvres

![Matthieu <](../../doc/Matthieu_Pritzy_test.jpg)

Installés depuis 2018 au cœur de la montagne bourbonnaise, nous élevons 80 chèvres en agriculture bio-logique. 
Au départ, notre troupeau était constitué de 40 chèvres alpines et de 40 saanens. Nous avons dès lors intégré la race massif central afin d'apporter de la rusticité a notre cheptel et d'accroître la qualité du lait.

Les chèvres pâturent d'avril à novembre sur un parcellaire de 25ha de prairies naturelles.
Nous récoltons (presque) la totalité du fourrage dont nous avons besoin pour couvrir leurs besoins a l'année. 
Elles sont également complémentées lors de la traite avec un aliment complet certifié AB provenant de la région AURA.

Depuis 2022, nous transformons la totalité de notre lait à la ferme


### Contrat

Vous pouvez télécharger les contrats au format [pdf] pour les imprimer et remplir en deux exemplaires.

> Un exemplaire devra être remis au producteur sur le lieu de livraison de l'Amap, accompagné de son règlement. Vous pouvez effectuer le paiement avec plusieurs  chèques. Dans ce cas, veuillez noter au dos de chaque chèque la date d'encaissement souhaitée.

[pdf]: ../doc/contrat_amap_Matthieu-Pritzy.pdf