Title: Hervé Brérat
Slug: Herve-Brerat
Authors: amapvichy
Summary: Porcs fermiers (élevés aux patates)

![Hervé <](../../doc/herve-brerat.jpg "Photo d'Hervé réalisée par Jean-Christophe Giraud – extraite du guide des producteurs de Vichy Communauté")

Je produis des porcs fermiers de 10 mois d'âge et propose du porc frais et des produits de salaison. Ma ferme de 1O hectares est située au pied de la montagne Bourbonnaise dans les monts de La Madeleine entre Lapalisse et Arfeuilles à 25 km de Vichy. Je produis de manière extensive: mon cheptel comprend 1O truies conduites sur paille ; le sevrage des porcelets se fait à 60 jours au lieu de 24 jours en système intensif. Leur alimentation ainsi que celle des porcs est à base de farine dés le plus jeune âge jusqu'à l'abattage. Ces farines sont fabriquées à partir de céréales bio par un moulin local. Élevés sur paille de la naissance à 80 Kg nos cochons s\'ébattent ensuite pendant 4 mois sur un parcours de 2 ha avant de rejoindre l\'étable sur paille pour la finition à base de farine et de pommes de terre. Mon but : un cochon de 10 à 15 mois d'âge pesant de 150 à 250 kg. Toute notre production est vendue en direct sur les marchés locaux et bien sûr, depuis Septembre 2005 à l'AMAP de Vichy (tous les jeudis) ce qui représente aujourd’hui 10% de nos ventes. L'AMAP, dans son fonctionnement de type associatif, reliant les consommateurs aux producteurs correspond pleinement à mes attentes : cela me permet un rapprochement avec les consommateurs (échange de point de vue sur les problèmes et les attentes de chacun. Leur soutien permet la sauvegarde de la ruralité, des productions agricoles locales. Je souhaite à l’avenir bien définir ce qu'est la qualité du produit pour en assurer son développement dans un contexte respectueux de l'environnement.


### Contrat

Vous pouvez télécharger les contrats au format [pdf] pour les imprimer et remplir en deux exemplaires.

> Un exemplaire devra être remis au producteur sur le lieu de livraison de l'Amap, accompagné de son règlement. Vous pouvez effectuer le paiement avec plusieurs  chèques. Dans ce cas, veuillez noter au dos de chaque chèque la date d'encaissement souhaitée.

[pdf]: ../doc/contrat_amap_herve-brerat.pdf
