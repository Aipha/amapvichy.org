Title: Estelle Venier
Slug: Estelle-Venier
Authors: amapvichy
Summary: Paysanne boulangère

Je suis Estelle VENIER, paysanne-boulangère récemment installée sur le GAEC
de la Licorne à Chouvigny, une ferme où travaillent déjà 4 associés sur les
ateliers suivants :
élevage de vaches laitières avec transformation en produits laitiers
(beurre, crème, fromages...), brebis viande, et maraîchage.

![Estelle <](../../doc/estelle-venier.jpg)

Pour l'atelier paysan-boulanger : nous avons semé nos céréales panifiables
cet automne (blé, seigle, petit épeautre) pour les moissonner cet été.
Nous aurons un moulin Astrié à partir du printemps prochain celui-ci est
en cours de fabrication mais les délais sont longs.
L'activité doit cependant démarrer, aussi jusqu'en septembre je me fournis
en farines bio auprès de paysans en Allier et en Creuse qui possèdent des
moulins et produisent des variétés anciennes qui m'intéressent pour leurs propriétés.

Je propose plusieurs types de pains : demi-complet nature, aux graines (lin/tournesol),
aux noix, seigle, petit épeautre... Ainsi que des pizzas fermières à partir
de cet été et des brioches.

Nous vendons aussi les pains au marché à la ferme les mercredis soir, et
les samedi matin toutes les semaines à la Grange à Jean à Péraclos (Chouvigny).

Estelle VENIER
Paysanne-boulangère
06 95 50 33 77
Marché à la Ferme : tous les Mercredis  17h-19h; tous les Samedis 10h-12h 
*Attention: trêve hivernale pour le marché du samedi, à partir du samedi 18 décembre*

Collectif Agricole de la Licorne
La Grange à Jean, Péraclos, 03450 Chouvigny


### Contrat

Vous pouvez télécharger les contrats au format [pdf] pour les imprimer et remplir en deux exemplaires.

> Un exemplaire devra être remis au producteur sur le lieu de livraison de l'Amap, accompagné de son règlement. Vous pouvez effectuer le paiement avec plusieurs  chèques. Dans ce cas, veuillez noter au dos de chaque chèque la date d'encaissement souhaitée.

[pdf]: ../doc/contrat_amap_estelle-venier.pdf
