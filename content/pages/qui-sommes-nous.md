Title: Présentation de l'AMAP
Slug: qui-sommes-nous
Authors: amapvichy
Summary: AMAP - Associations pour le maintien d’une agriculture paysanne - sont destinées à favoriser l’agriculture paysanne et biologique qui a du mal à subsister face à l’agro-industrie.

**Amapvichy** (Association pour le maintien d’une agriculture paysanne des pays de Vichy)
est une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901.
Vous pouvez consulter, en ligne, [nos statuts](../doc/statutsadoptes_ag2017.pdf).
Le principe est de créer un lien direct entre paysans et consommateurs,
qui s’engagent à acheter la production des premiers à un prix équitable et en payant par avance.

### Contexte
* disparition de l’agriculture paysanne, disparition de l’agriculture locale
    * chaque minute, une exploitation agricole disparaît en Europe
    * une ferme disparaît toutes les 15 minutes en France
* environnement, cancer, pesticide, mauvais traitement des animaux
* atomisation du citoyen, réduction à la consommation (produit, télé...)
* disparition de la convivialité et de la coopération (vs compétition)

**Nous voulons manger sainement, sans laisser d’empreinte sur l’environnement des générations futures,
sans exploiter les paysans et ceci pour le plus grand nombre**...

### Une solution : l’AMAP
*  Mise en avant de valeur de solidarité, des transparence et de convivialité
* La première solidarité est tournée vers le monde paysan
* chaque mangeur/consommateur d’un groupe va **pré-acheter** une partie de la récolte
* la récolte est partagée toutes les semaines durant la saison
* le coût de la part de récolte est déterminé par les **charges de l’exploitation**
* **conséquences** :
    * solidarité avec les aléas de production
    * pas de remboursement en cas d’absence à une distribution (un report de livraison peut être proposé par le ou la productrice s’il ou si elle est prévenu-e- suffisamment à l’avance, et si ses conditions matérielles et financières le permettent)
    * partage abondant quand la production est généreuse
    * transparence : on sait ce qu’il y a derrière le prix (une famille, des salariés...)
