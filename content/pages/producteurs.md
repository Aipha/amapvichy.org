Title: Producteurs
Slug: producteurs
Authors: amapvichy
Summary: Présentation des paysans partenaires de l'AMAP.

Pour s’y retrouver, il y a...

### Les producteurs hebdomadaires
* [Aurélie MATHIEU](Aurelie-Mathieu.html), maraîchère AB
* [Estelle VENIER](Estelle-Venier.html), paysanne boulangère

### Les producteurs bimensuels
* [Hervé BRERAT](Herve-Brerat.html), porc (élevé aux patates)
* [Pisciculture du Moulin Piat](Moulin-Piat.html) (Laurent et Guillaume), truites du Sichon
* [Héloïse - Les Terres Noires](Heloise-Les-Terres-Noires.html), œufs et conserves
* [Matthieu PRITZY](Matthieu-Pritzy.html), fromages de chèvres


Chaque **consom'acteur** et chaque producteur au moment de s'accorder, établissent **un contrat**
pour passer commande de produits sur plusieurs semaines, d'une part et garantir la livraison,
d'autre part... Le consom'acteur accepte de partager par le contrat **une part de risque**
du producteur (météo, modes culturaux, maladies, équipement, transformation...).

Le producteur est ainsi sécurisé (trésorerie) et moyennant quoi il pourra, *p. ex. et si possible*,
reporter d'autant la livraison ou substituer des produits à d'autres en cas d'échec d'une production.
